import { $ } from 'protractor';
var randomstring = require("randomstring");
import * as dataJson from '../data/testdata.json';

export class SignUpPage {

    mainTitle = $('body > app-root > app-auth-page > div > div > div > div > h1');
    signUpButton = $('body > app-root > app-auth-page > div > div > div > div > form > fieldset > button');
    username = $('body > app-root > app-auth-page > div > div > div > div > form > fieldset > fieldset:nth-child(1) > input');
    email = $('body > app-root > app-auth-page > div > div > div > div > form > fieldset > fieldset:nth-child(2) > input');
    password = $('body > app-root > app-auth-page > div > div > div > div > form > fieldset > fieldset:nth-child(3) > input');
    usernameTooShortMsg = $('body > app-root > app-auth-page > div > div > div > div > app-list-errors > ul > li');

    clickSignUp() {
        this.signUpButton.click();
    }

    random() {
        return randomstring.generate(8);
    }

    randomValidEmail() {
        return this.random() + '@' + this.random() + '.com';
    }

    randomValidPassword() {
        return this.random() + '*';
    }

    signUpValid() {
        this.username.sendKeys(this.random());
        this.email.sendKeys(this.randomValidEmail());
        this.password.sendKeys(this.randomValidPassword());
        this.clickSignUp();
    }

    signUpInvalidUsernameTooShort() {
        this.username.sendKeys((<any>dataJson).testdata.signup.invalidUsername.usernameLessThan1Char);
        this.email.sendKeys(this.randomValidEmail());
        this.password.sendKeys(this.randomValidPassword());
        this.clickSignUp();
    }

    signUpInvalidUsernameTooLong() {
        this.username.sendKeys((<any>dataJson).testdata.signup.invalidUsername.usernameMoreThan20Chars);
        this.email.sendKeys(this.randomValidEmail());
        this.password.sendKeys(this.randomValidPassword());
        this.clickSignUp();
    }

    signUpInvalidUsernameAlreadyRegistered() {
        this.username.sendKeys((<any>dataJson).testdata.signup.invalidUsername.usernameRegistered);
        this.email.sendKeys(this.randomValidEmail());
        this.password.sendKeys(this.randomValidPassword());
        this.clickSignUp();
    }

}