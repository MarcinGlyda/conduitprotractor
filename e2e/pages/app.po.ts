import { browser, $, ElementFinder } from 'protractor';
import { protractor } from 'protractor/built/ptor';

export class HomePage {

  mainTitle = $('body > app-root > app-home-page > div > div.banner > div > h1');
  globalFeedTab = $('body > app-root > app-home-page > div > div.container.page > div > div.col-md-9 > div > ul > li:nth-child(2) > a');
  yourFeedTab = $('body > app-root > app-home-page > div > div.container.page > div > div.col-md-9 > div > ul > li:nth-child(1) > a');
  tagsLink = $('body > app-root > app-home-page > div > div.container.page > div > div.col-md-3 > div > p');
  signInLink = $('body > app-root > app-layout-header > nav > div > ul > li:nth-child(2) > a');
  signUpLink = $('body > app-root > app-layout-header > nav > div > ul > li:nth-child(3) > a');
  yourFeedParagraph = $('body > app-root > app-home-page > div > div > div > div.col-md-9 > app-article-list > div:nth-child(2)');
  settings = $('body > app-root > app-layout-header > nav > div > ul > li:nth-child(3) > a');
  

  navigateTo() {
    return browser.get('http://localhost:4200');
  }

  waitForElementToBeVisible(element: ElementFinder, time: number){
    const expectedConditions = protractor.ExpectedConditions;
    browser.wait(expectedConditions.visibilityOf(element), time);
  }

  clickSignIn() {
    this.signInLink.click();
  }

  clickSignUp() {
    this.signUpLink.click();
  }

  clickYourFeed() {
    this.yourFeedTab.click();
  }

  async userSignedOut() {
    return await this.signInLink.getText() === 'Sign in';
  }
}
