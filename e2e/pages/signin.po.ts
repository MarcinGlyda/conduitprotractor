import { $, ElementFinder, protractor, browser } from 'protractor';
import * as dataJson from '../data/testdata.json';
var randomstring = require("randomstring");

export class SignInPage {

    mainTitle = $('body > app-root > app-auth-page > div > div > div > div > h1');
    signInButton = $('body > app-root > app-auth-page > div > div > div > div > form > fieldset > button');
    email = $('body > app-root > app-auth-page > div > div > div > div > form > fieldset > fieldset:nth-child(2) > input');
    password = $('body > app-root > app-auth-page > div > div > div > div > form > fieldset > fieldset:nth-child(3) > input');
    invalidEmailOrPassword = $('body > app-root > app-auth-page > div > div > div > div > app-list-errors > ul > li');
    userDetails = $('body > app-root > app-layout-header > nav > div > ul > li:nth-child(4) > a');
    userSettings = $('body > app-root > app-profile-page > div > div.user-info > div > div > div > a');
    signout = $('body > app-root > app-settings-page > div > div > div > div > button');

    waitForElementToBeVisible(element: ElementFinder, time: number) {
        const expectedConditions = protractor.ExpectedConditions;
        browser.wait(expectedConditions.visibilityOf(element), time);
    }

    signInValid() {
        this.email.clear();
        this.email.sendKeys((<any>dataJson).testdata.signin.valid.email);
        this.password.clear();
        this.password.sendKeys((<any>dataJson).testdata.signin.valid.password);
        this.signInButton.click();
    }

    signInInvalidEmail() {
        this.email.clear();
        this.email.sendKeys(randomstring.generate());
        this.password.clear();
        this.password.sendKeys((<any>dataJson).testdata.signin.valid.password);
        this.signInButton.click();
    }

    signInInvalidPassword() {
        this.email.clear();
        this.email.sendKeys((<any>dataJson).testdata.signin.valid.email);
        this.password.clear();
        this.password.sendKeys(randomstring.generate());
        this.signInButton.click();
    }

    signOut() {
        this.userDetails.click();
        this.userSettings.click();
        this.signout.click();
    }
}