import { HomePage } from "../pages/app.po";
import { SignUpPage } from "../pages/signup.po";
import { browser } from "protractor";
import { SignInPage } from "../pages/signin.po";
import { protractor } from "protractor/built/ptor";


describe('DEMO Sign up page', () => {
    let home: HomePage;
    let signup: SignUpPage;
    let signin: SignInPage;

    beforeEach(() => {
        home = new HomePage();
        signup = new SignUpPage();
        signin = new SignInPage();
        home.navigateTo();
    })

    it('should sign up successfully when credentials are valid', async () => {
        home.clickSignUp();
        signup.signUpValid();
        home.waitForElementToBeVisible(home.yourFeedParagraph, 1000);
        browser.actions().sendKeys(protractor.Key.ESCAPE);
        expect(await home.yourFeedParagraph.isDisplayed()).toBe(true);
    });

    fit('should fail signing up when username is too short', async () => {
        const isSignedOut = await home.userSignedOut();

        if (isSignedOut === false) {
            signin.signOut();
        }

        home.clickSignUp();
        signup.signUpInvalidUsernameTooShort();
        expect(await signup.usernameTooShortMsg.isDisplayed()).toBe(true);
        expect(await signup.usernameTooShortMsg.getText()).toContain('username can\'t be blank,is too short');
    });

    it('should fail signing up when username is too long', async () => {
        const isSignedOut = await home.userSignedOut();

        if (isSignedOut === false) {
            signin.signOut();
        }

        home.clickSignUp();
        signup.signUpInvalidUsernameTooLong();
        expect(signup.usernameTooShortMsg.isDisplayed()).toBe(true);
        expect(signup.usernameTooShortMsg.getText()).toContain('username is too long');
    });

    it('should fail signing up when username has been already registered', async () => {
        const isSignedOut = await home.userSignedOut();

        if (isSignedOut === false) {
            signin.signOut();
        }

        home.clickSignUp();
        signup.signUpInvalidUsernameAlreadyRegistered();
        expect(await signup.usernameTooShortMsg.isDisplayed()).toBe(true);
        expect(await signup.usernameTooShortMsg.getText()).toBe('username has already been taken');
    });
});