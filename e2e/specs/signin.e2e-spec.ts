import { SignInPage } from '../pages/signin.po';
import { HomePage } from '../pages/app.po';

describe('DEMO Sign in page', () => {
    let signin: SignInPage;
    let home: HomePage;

    beforeEach(() => {
        signin = new SignInPage();
        home = new HomePage();
        home.navigateTo();
    })

    it('should allow user to sign in - valid credentials', async () => {
        const isSignedOut = await home.userSignedOut();

        if (isSignedOut === false) {
            signin.signOut();
        }

        home.clickSignIn();
        signin.signInValid();
        expect(await home.settings.isDisplayed()).toBe(true);
        expect(await home.settings.getText()).toBe(' Settings');
    });

    it('should fail to sign in when the email is incorrect', async () => {
        const isSignedOut = await home.userSignedOut();

        if (isSignedOut === false) {
            signin.signOut();
        }

        home.clickSignIn();
        signin.signInInvalidEmail();
        expect(await signin.invalidEmailOrPassword.getText()).toBe('email or password is invalid');
    });

    it('should fail to sign in when the password is incorrect', async () => {
        const isSignedOut = await home.userSignedOut();

        if (isSignedOut === false) {
            signin.signOut();
        }

        home.clickSignIn();
        signin.signInInvalidPassword();
        expect(await signin.invalidEmailOrPassword.getText()).toBe('email or password is invalid');
    });
});