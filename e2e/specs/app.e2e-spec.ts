import { HomePage } from '../pages/app.po';
import { SignInPage } from '../pages/signin.po';
import { SignUpPage } from '../pages/signup.po';

describe('DEMO', () => {
  let page: HomePage;
  let signIn: SignInPage;
  let signUp: SignUpPage;

  beforeEach(() => {
    page = new HomePage();
    signIn = new SignInPage();
    signUp = new SignUpPage();
    page.navigateTo();
  });

  it('should display main page on load', async () => {
    expect(await page.mainTitle.getText()).toBe('conduit');
    expect(await page.globalFeedTab.getText()).toBe('Global Feed');
    expect(await page.tagsLink.getText()).toBe('Popular Tags');
  });

  describe('DEMO Navigation links', () => {

    it('should navigate to sign in page', async () => {
      page.clickSignIn();
      expect(await signIn.mainTitle.getText()).toBe('Sign in');
      expect(await signIn.signInButton.isDisplayed()).toBe(true);
    });

    it('should navigate to sign up page', async () => {
      page.clickSignUp();
      expect(await signUp.mainTitle.getText()).toBe('Sign up');
      expect(await signUp.signUpButton.isDisplayed()).toBe(true);
    });

    it('should navigate to sign in page, when I click Your Feed link', async () => {
      page.clickYourFeed();
      expect(await signUp.mainTitle.getText()).toBe('Sign in');
      expect(await signUp.signUpButton.isDisplayed()).toBe(true);
    });

    it('should navigate to your feed page when I click the Your Feed link', async () => {
      if (await page.userSignedOut()) {
        page.clickSignIn();
        signIn.signInValid();
      }
      page.waitForElementToBeVisible(page.yourFeedParagraph, 1000);
      expect(await page.yourFeedParagraph.getText()).toEqual('No articles are here... yet.');
    });
  });
});

